package com.template.views.profile

import android.view.View
import androidx.lifecycle.ViewModel
import com.template.MainActivity
import com.template.R

class ProfileVM : ViewModel() {

    fun onClick(view: View) {
        when (view.id) {

            R.id.navBar -> {
                MainActivity.navListener?.openDrawer()
            }
        }
    }
}