package com.template.views.chat

import android.view.View
import androidx.lifecycle.ViewModel
import com.template.utils.navigateBack

class ChatVM : ViewModel() {
    val adapter by lazy { ChatAdapter() }

    fun clickBack(view: View) {
        view.navigateBack()
    }

}