package com.template.views.support

import android.view.View
import androidx.lifecycle.ViewModel
import com.template.MainActivity
import com.template.R
import com.template.utils.navigateWithId

class SupportVM : ViewModel() {

    init {
        MainActivity.navListener?.isLockDrawer(true)
    }

    fun onClick(view: View) {
        when (view.id) {

            R.id.navBar -> {
                MainActivity.navListener?.openDrawer()
            }

            R.id.btContactUs -> {
                view.navigateWithId(R.id.action_support_to_contactUs)
            }
        }
    }

}
