package com.template.views.vehicleregistration

import android.view.View
import androidx.lifecycle.ViewModel
import com.template.MainActivity
import com.template.R
import com.template.utils.navigateBack
import com.template.utils.navigateWithId

class VehicleRegistrationVM : ViewModel() {

    init {
        MainActivity.navListener?.isLockDrawer(true)
    }

    fun onClick(view: View) {
        when (view.id) {

            R.id.ivBack -> {
                view.navigateBack()
            }

            R.id.btSubmit -> {
                view.navigateWithId(R.id.action_vehicleRegistration_to_welcome)
            }
        }
    }
}
