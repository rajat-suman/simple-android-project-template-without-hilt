package com.template.views.home

import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.ViewModel
import com.template.MainActivity
import com.template.R

class HomeVM : ViewModel() {

    var isOnline = ObservableBoolean(false)

    fun onClick(view: View) {
        when (view.id) {

            R.id.navBar -> {
                MainActivity.navListener?.openDrawer()
            }

            R.id.tvAccept -> {

            }

            R.id.tvIgnore -> {

            }

            R.id.toggleButton -> {
//                isOnline.set(!isOnline.get())
            }
        }
    }
}