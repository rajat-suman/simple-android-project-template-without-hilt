package com.template.views.contactus

import android.view.View
import androidx.lifecycle.ViewModel
import com.template.R
import com.template.utils.navigateBack

class ContactUsVM : ViewModel() {

    fun onClick(view: View) {
        when (view.id) {

            R.id.ivBack -> {
                view.navigateBack()
            }
        }
    }
}