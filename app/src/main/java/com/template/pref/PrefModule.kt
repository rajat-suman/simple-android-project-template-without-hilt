package com.template.pref

import android.content.Context
import android.content.SharedPreferences
import com.template.MainActivity

object PrefModule {

    val getPref: SharedPreferences by lazy {
        MainActivity.context.get()!!.getSharedPreferences(preferenceName, Context.MODE_PRIVATE)
    }

    val getEditor: SharedPreferences.Editor by lazy {
        getPref.edit()
    }


}